#!/usr/bin/fish

if [ (/bin/hostname) != "davidma.de" ]
    echo "Not on build server!"
    echo "Exiting..."
    exit 1
end

cp /home/david/repos/eng317/doc/eclipse-documentation/Output/print/book.pdf  /var/www/eng317/print/(date +"%m.%d.%Y.%H.%M.%S").pdf

echo "Done."
