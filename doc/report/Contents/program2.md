## Your Second Project - Taking in Input

1. Follow steps 1 through 14 in the "**Making your First Project**" section. With the project name of P3.
2. Create an temporary object where values will be assigned to before they are assigned to the main values. This is where you will use the ystem.in function to retrieve values for the users.


28. Expand **src** > **default package** > **P2.java**
29. Type out three objects that will hold three values (these variables will hold the values that the user enters).
    * Example: "**double grade1, grade2, grade3;**"

30. Create an temporary object where values will be assigned to before they are assigned to the main values. 
    * This is where you will uses the System.in function to retrieve values for the users.

    * Example: "**Scanner scanny = new Scanner(System.in);**"

    * You should also have an import package at the top of your program.  "**import java.util.Scanner;**" before the class. 

31. Create a prompt for the user to read using the System.out.print function.
    * Example: "**System.out.print(“Enter a grade");**”

32.  Assign the temporary value to one of the assigned values that was created in  step 19.
    * Example: "**grade1 = scanny.nextDouble();**"

33. Create a prompt for the user to read using the System.out.print function.
    * Example: "**System.out.print(“Enter a grade");**”

34.  Assign the temporary value to one of the assigned values that was created in  step 19.
    * Example: "**grade2 = scanny.nextDouble();**"

35. Create a prompt for the user to read using the System.out.print function.
    * Example: "**System.out.print(“Enter a grade");**”

36.  Assign the temporary value to one of the assigned values that was created in  step 19.
    * Example: "**grade3 = scanny.nextDouble();**"

37. Type " **System.out.println("Average " + (grade1 + grade2 + grade3) / 3);**"
    * NOTE: This function will add all three grades together and divide them by to find the average. The average will be displayed on the console.

NOTE: The code should look similar to the code below!

    import java.util.Scanner;
    public class StudentAverages {
        public static void main(String[] args) {
         double grade1, grade2, grade3;
         Scanner scanny = new Scanner(System.in);         
         int position = 0;
         System.out.print("Enter a grade: ");
         grade1 = scanny.nextDouble();
         System.out.print("Enter a grade: ");
         grade2 = scanny.nextDouble();        
         System.out.print("Enter a grade: ");
         grade3 = scanny.nextDouble(); 
         System.out.println("Average " + (grade1 + grade2 + grade3) / 3);
        }
    }
