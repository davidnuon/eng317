## Your First Project - Hello, World!


1. Enter the following line: **`System.out.println("Hello World.");`**
    - Make sure your code looks exactly like the one shown in bold above.
2. In the toolbar, click the **Run** button. 
    - A terminal should pop up, displaying the text *Hello World.*

Just to make sure, your code should look exactly like this:
	
    public class HelloWorld {
    	/**
    	  * @param args
    	 */
    	 public static void main(String[] args) {
    		// TODO: Auto-generated mehtod stub
    		System.out.println("Hello World.");			 
    	 }
    }
