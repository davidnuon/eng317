# Terminology

### General Terms
  1. **Eclipse** &mdash; is an integrated development environment. It contains a base workspace and an extensible plug-in system for customizing the environment. Written mostly in Java.  

  0. **Java** &mdash; is a computer programming language that is concurrent, class-based, object oriented and specifically designed to have as few implementation dependencies as possible.  

  0. **Program** &mdash; provides an application or machine with coded instructions for the automatic performance of a particular task.  

  0. **Code** &mdash; programming instructions.   

  0. **Project** &mdash; an individual or collaborative enterprise that is carefully planned and designed to achieve a particular aim.  

  0. **Run** &mdash; execution in computer and software engineering is the process by which a computer or a virtual machine performs the instructions of a computer program.  

  0. **Compiler** &mdash; is a program that processes statements written in a particular programming language and turns them into machine language or "code" that a computer's[ ](http://searchcio-midmarket.techtarget.com/definition/processor)processor uses.  

  0. **Console** &mdash; The console is used as a debugging aid that redirects any system errors and system outputs the console window. The Java console may be hidden or shown during start-up time, depending on the settings on the control panel  

### Explanation of Functions
  0. **System.out.println(" “ );** &mdash; System is a final class from java.lang package. Out is the reference of PrintStream class and a static member of System class. While println is a method of PrintStream class. This method prints out data and values.  

  0. **Scanner scanny = new Scanner(System.in);** &mdash; "scanny" the the variable that holds the value that the user enters from the console. Any variable after “scanner” becomes the variable that holds the information from the command line  

  0. **Scanner input** &mdash; "input" the the variable that holds the value that the user enters from the command line. Any variable after “scanner” becomes the variable that holds the information from the console  

  0. **CompareTo** &mdash; this function compares two objects and returns an integer. If the method returns a 0 it means the two objects are equal. If the function returns a negative, it means the second object is less than the first object. If the method returns 1, it means the second object is greater than the first.  

  0. **hasNext();** &mdash; this method returns true if this scanner has another token in its input. This method may block while waiting for input to scan. The scanner does not advance past any input.