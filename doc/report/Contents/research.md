#Additional research 

People might think that Starbucks is a franchise but technically they are not. Most stores are company-owned stores. The Starbucks Corporation finally started to adopt the franchise model to keep up with supply and demand. There is one difference from a typical franchise which is a franchiser must be obligated to build at least 20 Starbucks locations in the next 5 years. The franchiser must have around $700,000K in liquid assets (items that may be converted to cash quickly) and must have a background in the food service industry. The thought of owning and building out 20 Starbucks locations might deter some people from attempting to franchise. Basically, The Starbucks Corporation are very meticulous when choosing franchisers (Libava).

This information is relevant to our research because it shows that acquiring a Starbucks location is not as easy as people might think. The Starbucks Corporation goes through a great amount of requirements to decide if they want to build a store. Starbucks does a market study to the feasibly of the location being built. There are currently 45 franchise locations, with only 9 owners of all 45 locations. In conclusion, franchising a Starbucks location maybe possible but it requires many resources.  

![Rendering of what the proposed Starbucks might look like](sb.png)[1]

### Timeline
<table cellpadding="7" cellspacing="0" style="width:425px">
	<tbody>
		<tr>
			<td>
			<p>Task</p>
			</td>
			<td>
			<p>Task Name</p>
			</td>
			<td>
			<p>Duration</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>1</p>
			</td>
			<td>
			<p>Obtain permit</p>
			</td>
			<td>
			<p>0</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>2</p>
			</td>
			<td>
			<p>Sign building contract</p>
			</td>
			<td>
			<p>0</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>3</p>
			</td>
			<td>
			<p>Demo Building and backfill</p>
			</td>
			<td>
			<p>5</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>4</p>
			</td>
			<td>
			<p>Survey Bid with off sets</p>
			</td>
			<td>
			<p>1</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>5</p>
			</td>
			<td>
			<p>Set up Batter boards</p>
			</td>
			<td>
			<p>1</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>6</p>
			</td>
			<td>
			<p>Excavate Footings</p>
			</td>
			<td>
			<p>1</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>7</p>
			</td>
			<td>
			<p>Soils Engineer to review the site</p>
			</td>
			<td>
			<p>1</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>8</p>
			</td>
			<td>
			<p>Pour the foundation</p>
			</td>
			<td>
			<p>9</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>9</p>
			</td>
			<td>
			<p>Back fill &amp; compact</p>
			</td>
			<td>
			<p>3</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>10</p>
			</td>
			<td>
			<p>Under Slabs Utilities</p>
			</td>
			<td>
			<p>7</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>11</p>
			</td>
			<td>
			<p>Under Slab Gravel Venting</p>
			</td>
			<td>
			<p>2</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>12</p>
			</td>
			<td>
			<p>Pour Slab</p>
			</td>
			<td>
			<p>1</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>13</p>
			</td>
			<td>
			<p>Steel Erections</p>
			</td>
			<td>
			<p>3</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>14</p>
			</td>
			<td>
			<p>Framing</p>
			</td>
			<td>
			<p>15</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>15</p>
			</td>
			<td>
			<p>Eclectic</p>
			</td>
			<td>
			<p>5</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>16</p>
			</td>
			<td>
			<p>Block Benner</p>
			</td>
			<td>
			<p>15</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>17</p>
			</td>
			<td>
			<p>Hard Coat Stucco</p>
			</td>
			<td>
			<p>13</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>18</p>
			</td>
			<td>
			<p>Signage Install</p>
			</td>
			<td>
			<p>5</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>19</p>
			</td>
			<td>
			<p>External Wall Insulation</p>
			</td>
			<td>
			<p>3</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>20</p>
			</td>
			<td>
			<p>Glazing</p>
			</td>
			<td>
			<p>3</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>21</p>
			</td>
			<td>
			<p>Roofing - Sheet Metal</p>
			</td>
			<td>
			<p>10</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>22</p>
			</td>
			<td>
			<p>Drywall taping and Finishing</p>
			</td>
			<td>
			<p>8</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>23</p>
			</td>
			<td>
			<p>Painting</p>
			</td>
			<td>
			<p>3</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>24</p>
			</td>
			<td>
			<p>Acoustical Grid</p>
			</td>
			<td>
			<p>2</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>25</p>
			</td>
			<td>
			<p>Ceramic Tile</p>
			</td>
			<td>
			<p>7</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>26</p>
			</td>
			<td>
			<p>FRP</p>
			</td>
			<td>
			<p>2</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>27</p>
			</td>
			<td>
			<p>Cabinet and countertop install</p>
			</td>
			<td>
			<p>8</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>28</p>
			</td>
			<td>
			<p>Art work install</p>
			</td>
			<td>
			<p>2</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>29</p>
			</td>
			<td>
			<p>Seating and D&eacute;cor</p>
			</td>
			<td>
			<p>4</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>30</p>
			</td>
			<td>
			<p>Final machining and Eclectic</p>
			</td>
			<td>
			<p>13</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>31</p>
			</td>
			<td>
			<p>Final Clean Up</p>
			</td>
			<td>
			<p>3</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>32</p>
			</td>
			<td>
			<p>Final Inspection</p>
			</td>
			<td>
			<p>3</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>33</p>
			</td>
			<td>
			<p>Open for business</p>
			</td>
			<td>
			<p>1</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>&nbsp;</p>
			</td>
			<td>
			<p>&nbsp;</p>
			</td>
			<td>
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>&nbsp;</p>
			</td>
			<td>
			<p>Total Days:</p>
			</td>
			<td>
			<p>182</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p><a name="_GoBack"></a>The data collected displays the number of days it requires to open an Starbucks location. It displays the number of days it requires for the certain task. The construction could take up to 182 days (around 6 months). From this data if the campus starts construction on the Starbucks location on May 16, 2014, the location could be completed before the start of finals week in the winter (Wadsworth Development Group)</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
