## Your Third Project - Combining what we learned with Project 1 and 2

For the third program we will be printing out the hello world function with your name. The purpose of this project it to better familiarize yourself with the **scanner input**, **compareTo()** and **hasNext()** functions.

1. Follow steps 1 through 14 in the "**Making your First Project**" section. With the project name of P3.
2. Create an temporary object where values will be assigned to before they are assigned to the main values. This is where you will use the ystem.in function to retrieve values for the users.

    * Example: "** Scanner scanny = new Scanner(System.in);**"

3. Create a while loop with the variable "scanny" while using the **hasNext()** function.
    * Example "**while(scanny.hasNext())**"

4. Create a variable to hold the temporary value from the scanner
    * Example "**String foo = scanny.next()**;"

5. Print out the variables using System.out.print();
    * Example " **System.out.println("Hello world " + foo);**"

6. Check for the line "exit", since we’re going to need to stop eventually. When exit is taken as input, the program will stop.
    * Example:
 **   if (foo.compareTo("exit") == 0) {
   	 System.exit(0);
   }**

7. Close our blocks.

 }   }

{pagebreak}
NOTE: The code should look similar to the code below!


    import java.util.Scanner;
    public class P1 {
        /**
        * @param args
        */
        public static void main(String[] args) {
            // TODO Auto-generated method stub
            Scanner scanny = new Scanner(System.in);
            while(scanny.hasNext()) {
                 String foo = scanny.next();
                 System.out.println("Hello world " + foo);
                 // extra step
                 if (foo.compareTo("exit") == 0) {
                   System.exit(0);
                 }
            } 
        }
    }
