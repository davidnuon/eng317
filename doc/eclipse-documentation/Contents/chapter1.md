#Setting up your first Eclipse Project

##Taking a look at Eclipse
<img src="legend.jpg" />

  - **Project Explorer** &mdash; This panel lists all the projects you have current open in Eclipse.
  - **Class Windows** &mdash; This is where you will enter in code. Any instructions that refer to enter or editing code refer to this.
  - **Console Window** &mdash; This is where you will see the output of your program.
  - **Compile Button** &mdash; Clicking this button will compile and run your code.

{pagebreak}

## Running Eclipse
<table class="section-wrapper">
	<tr>
		<td class="content" markdown="1">
<ol markdown="1">
	<li>Locate and open the **Eclipse** program. 
	<ul>
		<li> popup window will appear.</li>
	</ul>
</li>
<li>Click **Browse** and locate a place to save the files.</li>
<li>Click **OK** to continue.</li>

</ol>
		</td>
		<td class="image"><img src="images/workspace.png" alt=""></td>
	</tr>
</table>

<!-- BREAK -->


## Getting Familiar with the Environment
<table class="section-wrapper">
	<tr>
		<td class="content" markdown="1">
			 	<ol>
			<li>
				<span>Make sure your screen looks like this, if not do the following:</span>

						<ol>
			<li>
				<span>Select from the Application Menu</span> <strong>Window &nbsp;</strong><span>&gt;</span> <strong>Open Perspective</strong> <span>&gt;</span> <strong>Other</strong>
			</li>
			<li>
				<span>Then select</span> <strong>Java (default)</strong> <span>from the list of options.</span>
			</li>
			<li>
				<span>Click</span> <strong>OK</strong><span>.</span>
			</li>
		</ol>
			</li>
		</ol>



		</td>
		<td class="image"><img src="images/image01.png" alt=""></td>
	</tr>
</table>


