# Making your First Projects #
<table class="section-wrapper">
	<tr>
		<td class="content" markdown="1">

<ol>
			<li>
				<span>&nbsp;</span><span>Click</span> <strong>File</strong><span>&nbsp;(in the top right hand corner).</span>
			</li>
			<li>
				<span>&nbsp;</span><span>Select</span> <strong>New</strong><span>.</span>
			</li>
		</ol>
		</td>
		<td class="image"><img src="images/image04.png" alt=""></td>
	</tr>
</table>
<table class="section-wrapper">
	<tr>
		<td class="content" markdown="1">
			


		<ol>
			<li>
				<span>&nbsp;</span><span>Click</span> <strong>New Java Project Project</strong><span>.</span>
			</li>
		</ol>
		<ul>
			<li>
				<span>A new window will appear.</span>
			</li>
		</ul>


		</td>
		<td class="image"><img src="images/image03.png" alt=""></td>
	</tr>
</table>
<table class="section-wrapper">
	<tr>
		<td class="content" markdown="1">
			
	
		<ol>
			<li>
				<span>&nbsp;</span><span>Enter the project name in the</span> <strong>Project name</strong><span>&nbsp;field.</span>
			</li>
			<li>
				<span>&nbsp;</span><span>Click</span> <strong>Finish</strong><span>&nbsp;to continue.</span>
			</li>
		</ol>

		</td>
		<td class="image"><img src="images/image05.png" alt=""></td>
	</tr>
</table>
<table class="section-wrapper">
	<tr>
		<td class="content" markdown="1">
		<ol>
		<li>
				<span>&nbsp;</span><span>Click</span> <strong>Windows</strong><span>&nbsp;&gt;</span> <strong>&nbsp;Show View</strong><span>&nbsp;&gt;</span> <strong>Project Explorer</strong><span>.</span><span>&nbsp;</span>
			</li>
		</ol>
		<ul>
			<li>
				<span>A column on the left hand side should appear labeled as</span> <strong>Project Explorer</strong><span>.</span>
			</li>
		</ul>


		</td>
		<td class="image"><img src="images/image06.png" alt=""></td>
	</tr>
</table>
<table class="section-wrapper">
	<tr>
		<td class="content" markdown="1">
				<ol>
			<li>
				<span>&nbsp;</span><span>Find the Project created in the</span> <strong>Project Explorer</strong><span>.</span>
			</li>
			<li>
				<span>Highlight the project and right click.</span>
			</li>
			<li>
				<span>&nbsp;</span><span>Highlight</span> <strong>New</strong><span>&nbsp;and click</span> <strong>Class</strong><span>.</span><span>&nbsp;</span>
			</li>
		</ol>
		<ul>
			<li>
				<span>A new window will appear.</span>
			</li>
		</ul>


		</td>
		<td class="image"><img src="images/image07.png" alt=""></td>
	</tr>
</table>
<table class="section-wrapper">
	<tr>
		<td class="content" markdown="1">
					<ol>
			<li>
				<span>&nbsp;</span><span>Find the</span> <strong>Name</strong><span>&nbsp;field and enter the name of the Class.</span>
			</li>
		</ol>
		<ul>
			<li>
				<span>(insert information of the class)</span>
			</li>
		</ul>
		<ol>
			<li>
				<span>Check the box that says “</span><span>public static void main(String[] args)</span><span>”.</span>
			</li>
			<li>
				<span>Press</span> <strong>Finish</strong><span>&nbsp;to continue.</span>
			</li>
					<ul>
			<li>
				<span>Your new class should be displayed on the screen.</span>
			</li>
		</ul>
		</ol>

		<ol>
			<li>
				<span>&nbsp;</span><span>Expand the folder with the name of your project.</span>
			</li>
			<li>
				<span>Expand</span> <strong>src</strong><span>&nbsp;&gt;</span> <strong>default package</strong><span>&nbsp;&gt;</span> <strong>HelloWorld.java</strong>
			</li>
		</ol>
		</td>
		<td class="image"><img src="images/image08.png" alt=""></td>
	</tr>
</table>