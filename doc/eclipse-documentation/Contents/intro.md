The goal of this tutorial is to instruct the readers how to set up an Eclipse project, get them familiar with the environment, and to also program their first projects. Reading this could help you get a jump start on how to understand basic programming. It would be an advantage to learn this if you’re interested in learning how to program and don’t know where to start off, especially for incoming students to the computer science field. This is beneficial to learn because Eclipse is a simple application that is quick and easy to learn with our tutorial. The programs that are taught in our tutorial are informational and useful to apply to future programs. Also the knowledge you gain from this would give you a large push in the right direction of programming. 



